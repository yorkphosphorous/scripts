from typing import List
from multiprocessing import Pool, TimeoutError
from pathlib import Path
from subprocess import run
from shutil import copy



def work(sample):
    source,dest = sample
    dest = Path(dest)
    dest = Path(dest.parent,'{}.ogg'.format(dest.stem))
    # example command to base this off
    # ffmpeg -i '/home/arbition/Music/Neil Cicierega/Mouth Silence/02 - Rollercloser.flac' -c:a libopus -b:a 96k /tmp/o.ogg
    run(['ffmpeg','-i',source,'-c:a','libopus','-b:a','96k',dest])

audio_file_extensions = [
    ".mp3",
    ".flac",
    ".m4a",
    ".webm",
    ".ogg",
    ".ape",
    ".wav",
    ".opus",
    ".wma",
    ".avi"
]
process_count = 5
if __name__ == "__main__":
    """
    Converts all identified audio files into opus contained in ogg, and recreates the folder structure at dest
    """
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("source_folder")
    parser.add_argument("dest_folder")
    args = parser.parse_args()
    audio_paths = []
    unique_extensions = set()
    for path in Path(args.source_folder).glob("**/*"):
        relpath = path.relative_to(Path(args.source_folder))
        if path.is_dir():
            pass
        else:
            destfolder = Path(args.dest_folder,relpath.parent)
            if not destfolder.exists():
                destfolder.mkdir(parents=True,exist_ok=True)
            destfile = Path(args.dest_folder,relpath)
            ext = destfile.suffix.lower()
            if ext in audio_file_extensions:
                audio_paths.append((path,destfile,))
            else:
                unique_extensions.add(ext)
                #copy(path,destfile)




    with Pool(processes=process_count) as pool:
        multiple_results = [pool.apply_async(work, ([pathish])) for pathish in audio_paths]
        dataarr = [res.get() for res in multiple_results]
