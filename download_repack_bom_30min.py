import io
import json
import pprint
import re
import subprocess
import tarfile
from ftplib import FTP
from tarfile import TarInfo

import pandas as pd


def untar_read_json (streamObj):
    loaded_json = {}
    streamObj.seek(0)
    with tarfile.open(fileobj=streamObj,mode='r') as tarin:
        json_members = [member for member in tarin.getmembers() if member.name.endswith(".json")]
        for member in json_members:
            
            gotten_member = tarin.extractfile(member)

            loaded_json[member.name] = json.load(gotten_member)
    return loaded_json


def station_json_to_dataframes(loaded_json):
    dfs = {}
    sitedetails = {}
    histprod = set()
    for station in loaded_json.values():
        if len(station["observations"]["data"]) == 0:
            continue
        df = pd.DataFrame(station["observations"]["data"])
        try:
            wmos = df["wmo"].unique()
            if len(wmos) != 1:
                print("multiple station???", wmos)
                continue
            wmo = int(wmos[0])
            names = df["name"].unique()
            if len(names) != 1:
                print("more than one name???")
                continue
            elif names[0] != station["observations"]["header"][0]["name"]:
                print("name mismatch!")
                continue
            name = station["observations"]["header"][0]["name"]
            tznames = df["time_zone_name"].unique()
            if len(tznames) != 1:
                print("Time zone change wtf!?!")
                continue
            tzname = tznames[0]

            hist_products = df["history_product"].unique()
            for hp in hist_products:
                histprod.add(hp)

            sitedetails[wmo] = {"name":name,"time_zone_name": tzname}
            utc_col = df[u"aifstime_utc"]
            df['time'] = pd.to_datetime(utc_col, utc=True, format='%Y%m%d%H%M%S')


            df = df.set_index(keys=[u"wmo",u"time"])
            df = df.drop([u"sort_order",u"aifstime_utc",u"aifstime_local",u"time_zone_name",u"name",u"TDZ",u"history_product"],axis=1)
            dfs[station["observations"]["header"][0]["name"]] = df
        except KeyError as e:
            print(station["observations"]["header"][0]["name"])
            print(e)
            continue
    histprod = list(histprod)
    if len(histprod) != 1:
        print("more than one (or no) hist prod in file!!!{}".format(histprod))
        print(loaded_json)
    else:
        histprod = histprod[0]
        
    df = pd.concat(list(dfs.values()))
    last_time = df.index.get_level_values(level="time").max().strftime("%Y-%m-%dT%H_%M_%SZ")
    return (last_time,sitedetails,histprod,dfs)




def repackage_as_csv(last_time,sitedetails,histprod,dfs):
    


    descriptor_bytes = io.BytesIO(bytearray(json.dumps(sitedetails),'utf-8'))
    descriptor_tarinfo = TarInfo('SouthAustralia-wmo-details-{}.json'.format(last_time))
    descriptor_tarinfo.size = len(descriptor_bytes.getvalue())

    inmem_tar = io.BytesIO()

    with tarfile.open(fileobj=inmem_tar,mode="w") as tarw:
        for name,df in dfs.items():
            name = re.sub('[^a-zA-Z0-9 -\(\)]','_',name)
            csv_bytes = io.StringIO()
            df.to_csv(path_or_buf=csv_bytes)
            csv_bytes = io.BytesIO(bytearray(csv_bytes.getvalue(),'utf-8'))
            data_tarinfo = TarInfo('{}-{}.csv'.format(name,last_time))
            data_tarinfo.size = len(csv_bytes.getvalue())
            tarw.addfile(data_tarinfo,fileobj=csv_bytes)
            
        #tarw.addfile(descriptor_tarinfo,fileobj=descriptor_bytes)

    subprocess.run(args=['zstd','-f','-19','-T1','-','-o',"/tmp/{}-{}.tar.zst".format(histprod,last_time)], input=inmem_tar.getvalue())
    

if __name__ == "__main__":

    filelist = []
    data_tuples = []
    def linecmd(line):
        if ("60910.tgz" in line):
            filelist.append(line)
        

    desired_dir= 'anon/gen/fwo'
    ftp = FTP('ftp.bom.gov.au')
    try:
        ftpres = ftp.login()
        print(ftpres)
        ftpres = ftp.cwd(desired_dir)
        print(ftpres)
        ftpres = ftp.retrlines('NLST',linecmd)
        print(ftpres)
        for filename in filelist:
            with io.BytesIO() as fp:
                print("retrieving {}".format(filename))
                ftpres = ftp.retrbinary('RETR {}'.format(filename), fp.write)
                print(ftpres)
                dataframe_dict = station_json_to_dataframes(untar_read_json(fp))
                data_tuples.append(dataframe_dict)
    finally:
        ftpres = ftp.quit()
        print(ftpres)

    for processed_file_desc in data_tuples:
        repackage_as_csv(*processed_file_desc)
